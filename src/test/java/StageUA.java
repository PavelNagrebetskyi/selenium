import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.WebDriverWait;
public class StageUA extends OpenChrome {
    OpenChrome stage ;

    void openStageUA() throws InterruptedException {
        stage.createInstance();
        stage = new OpenChrome();
        stage.chromeEmpryPage();
        String stageLink = "https://tickets.ua.default.staging.ttndev.com/";
        String refID="/?refid=280";
        stage.OpenLink(stageLink+refID);
        new WebDriverWait(driverChrome, 300000).until(
                driverChrome -> ((JavascriptExecutor) driverChrome).executeScript("return document.readyState").equals("complete"));

        driverChrome.quit();
    }
}
