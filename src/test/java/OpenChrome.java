import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class OpenChrome {
    public WebDriver driverChrome;

    public static void createInstance(){
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

    }
    public void chromeEmpryPage() throws InterruptedException {


        System.setProperty("webdriver.chrome.driver", "D:\\chromedriver\\chromedriver.exe");
        driverChrome = new ChromeDriver();
       // Thread.sleep(5000);  // Let the user actually
    }

    void OpenLink(String link) {
        driverChrome.get(link);
    }

    private void getCurrentUrl() {
        driverChrome.getCurrentUrl();
    }

    public void quitChrome(){
        driverChrome.quit();
    }
}
